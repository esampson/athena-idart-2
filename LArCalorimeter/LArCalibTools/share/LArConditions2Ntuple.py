#!/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

from AthenaConfiguration.ComponentFactory import CompFactory


if __name__=='__main__':

  import os,sys
  import argparse

  # now process the CL options and assign defaults
  parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('-r','--run', dest='run', default=0x7fffffff, help='Run number', type=int)
  parser.add_argument('--sqlite', dest='sqlite', default=None, help='sqlite file to read from (default: oracle)', type=str)
  parser.add_argument('-t','--tag',dest='dbtag',default=None,help="Global conditions tag", type=str)
  parser.add_argument('-f','--ftag',dest='ftag',default=None,help="folder tag suffig", type=str)
  parser.add_argument('-o','--out', dest='out', default="LArConditions.root", help='Output root file', type=str)
  parser.add_argument('--ofcfolder',dest='ofcfolder',default="", help="OFC flavor",type=str)
  parser.add_argument('-s','--isSC', dest='isSC', action='store_true', default=False, help='is SC?')
  parser.add_argument('-m','--isMC', dest='isMC', action='store_true', default=False, help='is MC?')

  parser.add_argument("--objects",dest="objects",default="PEDESTAL,RAMP",help="List of conditions types to be dumped",type=str)
  parser.add_argument("--folders",dest="folders",default="/LAR/ElecCalibFlat/Pedestal,/LAR/ElecCalibFlat/Ramp",help="List of folders to be taken from sqlite",type=str)
  parser.add_argument('--offline',dest="offline", action='store_true', default=False, help='is offline folder?')
   
  args = parser.parse_args()
  if help in args and args.help is not None and args.help:
    parser.print_help()
    sys.exit(0)


  #Translation table ... with a few potential variant spellings
  objTable={"RAMP":"Ramp",
            "DAC2UA":"DAC2uA",
            "DACUA":"DAC2uA",
            "PEDESTAL":"Pedestal",
            "PED":"Pedestal",
            "UA2MEV":"uA2MeV",
            "UAMEV":"uA2MeV",
            "MPHYSOVERMCAL":"MphysOverMcal",
            "MPHYSMCAL":"MphysOverMcal",
            "MPMC":"MphysOverMcal",
            "OFC":"OFC",
            "SHAPE":"Shape",
            "HVSCALECORR":"HVScaleCorr",
            "HVSCALE":"HVScaleCorr",
            "FSAMPL":"fSampl",
            "AUTOCORR":"AutoCorr",
            "AC":"AutoCorr",
            "CALIWAVE":"CaliWave",
            "PHYSWAVE":"PhysWave",
            "OFCCALI":"OFCCali"
          }

  objects=set()
  objectsOnl=set() 
  for obj in args.objects.split(","):
    objU=obj.upper()
    if objU not in objTable:
      print("ERROR: Unknown conditions type",obj)
      sys.exit(0)

    objects.add(objTable[objU])
    if "OFCCALI" not in obj.upper() and 'WAVE' not in obj.upper() and not args.offline:
       objectsOnl.add(objTable[objU])
    
  flds=set()
  for fld in args.folders.split(","):
     flds.add(fld)
 
  from AthenaConfiguration.AllConfigFlags import initConfigFlags
  flags=initConfigFlags()
  from LArCalibProcessing.LArCalibConfigFlags import addLArCalibFlags
  addLArCalibFlags(flags, args.isSC)
   
  flags.Input.RunNumbers=[args.run]
  from AthenaConfiguration.TestDefaults import defaultGeometryTags
  flags.GeoModel.AtlasVersion=defaultGeometryTags.RUN3

  flags.Input.Files=[]

  flags.Input.isMC=args.isMC
  flags.LArCalib.isSC=args.isSC

  flags.LAr.doAlign=False

  flags.LAr.OFCShapeFolder=args.ofcfolder

  from AthenaConfiguration.Enums import LHCPeriod
  if flags.Input.RunNumbers[0] < 222222:
    #Set to run1 for early run-numbers
    flags.GeoModel.Run=LHCPeriod.Run1 
    flags.IOVDb.DatabaseInstance="OFLP200" if flags.Input.isMC else "COMP200" 
  else:
    flags.GeoModel.Run=LHCPeriod.Run2
    flags.IOVDb.DatabaseInstance="OFLP200" if flags.Input.isMC else "CONDBR2"

  if args.dbtag:
    flags.IOVDb.GlobalTag=args.dbtag
  elif flags.Input.isMC:
    flags.IOVDb.GlobalTag="OFLCOND-MC16-SDR-20"
  elif flags.IOVDb.DatabaseInstance == "COMP200":
    flags.IOVDb.GlobalTag="COMCOND-BLKPA-RUN1-09"
  else: 
    flags.IOVDb.GlobalTag="CONDBR2-ES1PA-2024-01"

  #flags.Exec.OutputLevel=1

  if (args.sqlite):
    flags.IOVDb.SqliteInput=args.sqlite
    flags.IOVDb.SqliteFolders=tuple(flds)
  if len(objects)!=len(objectsOnl):
    flags.IOVDb.DBConnection="COOLOFL_LAR/CONDBR2" 

  flags.lock()
  
  from AthenaConfiguration.MainServicesConfig import MainServicesCfg
  cfg=MainServicesCfg(flags)

  #MC Event selector since we have no input data file
  from McEventSelector.McEventSelectorConfig import McEventSelectorCfg
  cfg.merge(McEventSelectorCfg(flags,
                               EventsPerRun      = 1,
                               FirstEvent	      = 1,
                               InitialTimeStamp  = 0,
                               TimeStampInterval = 1))



  #Get LAr basic services and cond-algos
  from LArGeoAlgsNV.LArGMConfig import LArGMCfg
  cfg.merge(LArGMCfg(flags))

  if flags.LArCalib.isSC:
    #Setup SuperCell cabling
    from LArCabling.LArCablingConfig import LArOnOffIdMappingSCCfg, LArCalibIdMappingSCCfg, LArLATOMEMappingCfg
    cfg.merge(LArOnOffIdMappingSCCfg(flags))
    cfg.merge(LArCalibIdMappingSCCfg(flags))
    cfg.merge(LArLATOMEMappingCfg(flags))
    if not args.offline:
       from LArConfiguration.LArElecCalibDBConfig import LArElecCalibDBSCCfg
       cfg.merge(LArElecCalibDBSCCfg(flags,objectsOnl))
  else: 
    #Setup regular cabling
    from LArCabling.LArCablingConfig import LArOnOffIdMappingCfg, LArCalibIdMappingCfg
    cfg.merge(LArOnOffIdMappingCfg(flags))
    cfg.merge(LArCalibIdMappingCfg(flags))
    if not args.offline:
       from LArConfiguration.LArElecCalibDBConfig import LArElecCalibDBCfg
       cfg.merge(LArElecCalibDBCfg(flags,objectsOnl))
  
  from LArBadChannelTool.LArBadChannelConfig import LArBadChannelCfg
  cfg.merge(LArBadChannelCfg(flags, isSC=flags.LArCalib.isSC))

  bcKey = "LArBadChannelSC" if flags.LArCalib.isSC else "LArBadChannel"

  if "Pedestal" in objects:
    ckey = "LArPedestalSC" if flags.LArCalib.isSC else "LArPedestal"
    cfg.addEventAlgo(CompFactory.LArPedestals2Ntuple(ContainerKey = ckey,
                                                        AddFEBTempInfo = False, 
                                                        AddCalib = True,
                                                        isSC = flags.LArCalib.isSC,
                                                        BadChanKey = bcKey
                                                      ))
                      
  if "AutoCorr" in objects:
    ckey="LArAutoCorrSC" if flags.LArCalib.isSC else "LArAutoCorr"
    cfg.addEventAlgo(CompFactory.LArAutoCorr2Ntuple(ContainerKey = "LArAutoCorrSym" if flags.Input.isMC else ckey,
                                                    AddFEBTempInfo = False, 
                                                    AddCalib = True,
                                                    isSC = flags.LArCalib.isSC,
                                                    BadChanKey = bcKey
                                                  ))
  if "Ramp" in objects:
    ckey = "LArRampSC" if flags.LArCalib.isSC else "LArRamp"
    cfg.addEventAlgo(CompFactory.LArRamps2Ntuple(RampKey="LArRampSym" if flags.Input.isMC else ckey,
                                                 AddFEBTempInfo = False, 
                                                 AddCalib = True,
                                                 isSC = flags.LArCalib.isSC,
                                                 BadChanKey = bcKey
                                               ))
  
  if "OFC" in objects:
    if args.offline: 
       from IOVDbSvc.IOVDbSvcConfig import addFolders
       for fld in flds:
          if 'OFC' in fld:
             if args.ftag:
                cfg.merge(addFolders(flags,fld,tag="".join(foldername.split('/')) + args.ftag))
             else:
                cfg.merge(addFolders(flags,fld))
             ckey= 'LArOFC' if '1phase' in fld else 'LArLArOFCPhys4samples'
             break
    else:         
       ckey = "LArOFCSC" if flags.LArCalib.isSC else "LArOFC"
    cfg.addEventAlgo(CompFactory.LArOFC2Ntuple(AddFEBTempInfo   = False,   
                                               ContainerKey=ckey,
                                               isSC = flags.LArCalib.isSC,
                                               BadChanKey = bcKey
                                             ))

  if "OFCCali" in objects:
    if args.offline: 
       from IOVDbSvc.IOVDbSvcConfig import addFolders
       for fld in flds:
          if 'OFC' in fld:
             if args.ftag:
                cfg.merge(addFolders(flags,fld,tag="".join(foldername.split('/')) + args.ftag))
             else:
                cfg.merge(addFolders(flags,fld))
             ckey= 'LArOFC' if '1phase' in fld else 'LArOFC'
             break
    else:         
       ckey = "LArOFCSCCali" if flags.LArCalib.isSC else "LArOFCCali"
       fldr = "/LAR/ElecCalibFlatSC/OFCCali" if flags.LArCalib.isSC else "/LAR/ElecCalibFlat/OFCCali"
       dbString = "<db>sqlite://;schema="+args.sqlite+";dbname=CONDBR2" if args.sqlite else  "<db>COOLONL_LAR/CONDBR2</db>"
       from IOVDbSvc.IOVDbSvcConfig import addFolders
       cfg.merge(addFolders(flags,fldr,detDb=dbString,className="CondAttrListCollection"))
       LArOFCSCCondAlg  =  CompFactory.getComp("LArFlatConditionsAlg<LArOFCSC>")("LArOFCSCCaliCondAlg")
       LArOFCSCCondAlg.ReadKey=fldr
       LArOFCSCCondAlg. WriteKey=ckey
       cfg.addCondAlgo(LArOFCSCCondAlg)

    cfg.addEventAlgo(CompFactory.LArOFC2Ntuple("LArOFC2NtupleCali",
                                               AddFEBTempInfo   = False,   
                                               ContainerKey=ckey,
                                               NtupleName="OFCCali",
                                               isSC = flags.LArCalib.isSC,
                                               BadChanKey = bcKey
                                             ))


  if "Shape" in objects:
    ckey = "LArShapeSC" if flags.LArCalib.isSC else "LArShape"
    if flags.Input.isMC:
       ckey="LArShapeSym"
    cfg.addEventAlgo(CompFactory.LArShape2Ntuple(ContainerKey=ckey,
                                                 AddFEBTempInfo   = False,   
                                                 AddCalib = True,
                                                 isSC = flags.LArCalib.isSC,
                                                 BadChanKey = bcKey
               
                                               ))
  if "MphysOverMcal" in objects:
    cfg.addEventAlgo(CompFactory.LArMphysOverMcal2Ntuple(ContainerKey   = "LArMphysOverMcalSC" if flags.LArCalib.isSC else "LArMphysOverMcal",
                                                         AddFEBTempInfo   = False,
                                                         AddCalib = True,
                                                         isSC = flags.LArCalib.isSC,
                                                         BadChanKey = bcKey
                                                       ))

  #ADC2MeV and DACuA are handled by the same ntuple dumper
  if "DAC2uA" in objects or "uA2MeV" in objects:
    uackey = "LAruA2MeVSC" if flags.LArCalib.isSC else "LAruA2MeV"
    dackey = "LArDAC2uASC" if flags.LArCalib.isSC else "LArDAC2uA"
    ua2MeVKey="LAruA2MeVSym" if flags.Input.isMC else uackey
    dac2uAKey="LArDAC2uASym" if flags.Input.isMC else dackey

    cfg.addEventAlgo(CompFactory.LAruA2MeV2Ntuple(uA2MeVKey=ua2MeVKey if "uA2MeV" in objects else "",
                                                  DAC2uAKey=dac2uAKey if "DAC2uA" in objects else "",
                                                  isSC = flags.LArCalib.isSC,
                                                  BadChanKey = bcKey
                                                ))
    

  if "HVScaleCorr" in objects:
    cfg.addEventAlgo(CompFactory.LArHVScaleCorr2Ntuple(ContainerKey= "LArHVScaleCorrSC" if flags.LArCalib.isSC else "LArHVScaleCorr",
                                                       AddFEBTempInfo = False,
                                                       isSC = flags.LArCalib.isSC,
                                                       BadChanKey = bcKey
                                                     ))

  if "fSampl" in objects:
    cfg.addEventAlgo(CompFactory.LArfSampl2Ntuple(ContainerKey="LArfSamplSC" if flags.LArCalib.isSC else "LArfSamplSym",
                                                  isSC=flags.LArCalib.isSC
                                                ))

  if "CaliWave" in objects:
    if flags.Input.isMC:
       print('No CaliWave in MC')
    else:   
       fld = "/LAR/ElecCalibOflSC/CaliWaves/CaliWave" if flags.LArCalib.isSC else "/LAR/ElecCalibOfl/CaliWaves/CaliWave"   
       from IOVDbSvc.IOVDbSvcConfig import addFolders
       #cfg.merge(addFolders(flags,fld,className="LArCaliWaveContainer"))
       cfg.merge(addFolders(flags,fld))
       cfg.addEventAlgo(CompFactory.LArCaliWaves2Ntuple(KeyList = ["LArCaliWave"],
                                                 NtupleName = "CALIWAVE",
                                                 AddFEBTempInfo = False,   
                                                 SaveDerivedInfo = True,
                                                 AddCalib = True,
                                                 SaveJitter = True,
                                                 isFlat = False,
                                                 isSC = flags.LArCalib.isSC,
                                                 BadChanKey = bcKey
                                               ))

  if "PhysWave" in objects:
    if flags.Input.isMC:
       print('No PhysWave in MC yet')
    else:   
       fld = "/LAR/ElecCalibOflSC/PhysWaves/RTM" if flags.LArCalib.isSC else "/LAR/ElecCalibOfl/PhysWaves/RTM"   
       from IOVDbSvc.IOVDbSvcConfig import addFolders
       #cfg.merge(addFolders(flags,fld,className="LArPhysWaveContainer"))
       cfg.merge(addFolders(flags,fld))
       cfg.addEventAlgo(CompFactory.LArPhysWaves2Ntuple(KeyList = ["LArPhysWave"],
                                                 NtupleName = "PHYSWAVE",
                                                 AddFEBTempInfo = False,   
                                                 SaveDerivedInfo = True,
                                                 AddCalib = True,
                                                 isFlat = False,
                                                 isSC = flags.LArCalib.isSC,
                                                 BadChanKey = bcKey
                                               ))

  rootfile=args.out
  if os.path.exists(rootfile):
    os.remove(rootfile)
  cfg.addService(CompFactory.NTupleSvc(Output = [ "FILE1 DATAFILE='"+rootfile+"' OPT='NEW'" ]))
  cfg.setAppProperty("HistogramPersistency","ROOT")

  if args.dbtag and 'CALIB' in args.dbtag:
     cfg.getService("IOVDbSvc").DBInstance=""
    
  cfg.run(1)
  sys.exit(0)
  
