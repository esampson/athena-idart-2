/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONREADOUTGEOMETRYR4_RADIALSTRIPDESIGN_ICC
#define MUONREADOUTGEOMETRYR4_RADIALSTRIPDESIGN_ICC
#include <GaudiKernel/SystemOfUnits.h>
/// Helper macro to shift the strip number and check that's valid
#define CHECK_STRIPRANGE(STRIP_NUM)                                                           \
    const int stripCh = (STRIP_NUM - firstStripNumber());                                     \
    if (stripCh < 0 ||  stripCh >= numStrips()) {                                             \
            ATH_MSG_WARNING(__func__<<"() -- Invalid strip number given "                     \
                         <<STRIP_NUM<<" allowed range ["                                      \
                         <<firstStripNumber()<<";"<<(firstStripNumber() +numStrips())<<"]");  \
           return Amg::Vector2D::Zero();                                                      \
    }
    

namespace MuonGMR4{
    using CheckVector2D = RadialStripDesign::CheckVector2D;
    inline int RadialStripDesign::numStrips() const { return m_strips.size() -1; }
    
    inline CheckVector2D RadialStripDesign::leftInterSect(int stripNum, bool /*uncapped*/) const {
        /// Calculate the strip width center at the bottom edge
        return std::make_optional<Amg::Vector2D>(0.5*(m_strips[stripNum].topMounting()  + 
                                                      m_strips[stripNum +1].topMounting()));
    }


    inline CheckVector2D RadialStripDesign::rightInterSect(int stripNum, bool /*uncapped*/) const {
        /// Calculate the strip width center at the top edge
        return std::make_optional<Amg::Vector2D>(0.5*(m_strips[stripNum].bottomMounting()  + 
                                                      m_strips[stripNum +1].bottomMounting()));
    }

    inline double RadialStripDesign::stripLength(int stripNumb) const {
        const int stripCh = (stripNumb - firstStripNumber());
        if (stripCh < 0 ||  stripCh >= numStrips()) {
            ATH_MSG_WARNING(__func__<<"() -- Invalid strip number given "
                         <<stripNumb<<" allowed range [" 
                         <<firstStripNumber()<<";"<<(firstStripNumber() +numStrips())<<"]");
            return 0.;
        }
        return m_strips[stripCh].fromBottomToTop().mag();
    }
    inline Amg::Vector2D RadialStripDesign::stripLeftEdge(int stripNumber) const{
        CHECK_STRIPRANGE(stripNumber);
        return m_strips[stripCh].fromBottomToTop().unit();
    }
    inline Amg::Vector2D RadialStripDesign::stripRightEdge(int stripNumber) const {
        CHECK_STRIPRANGE(stripNumber);
        return m_strips[stripCh + 1].fromBottomToTop().unit();
    }
    inline Amg::Vector2D RadialStripDesign::stripDir(int stripNumber) const {
        CHECK_STRIPRANGE(stripNumber);  
        return (m_strips[stripCh].fromBottomToTop() + m_strips[stripCh+1].fromBottomToTop()).unit();
    }
    inline Amg::Vector2D RadialStripDesign::stripNormal(int stripNumber) const {
        const Eigen::Rotation2D rotMat{(m_reversedStripOrder ? 90 : -90) * Gaudi::Units::deg};
        return rotMat * stripDir(stripNumber);
    }

    inline Amg::Vector2D RadialStripDesign::stripEdges::bottomMounting() const{
        return parent.cornerBotLeft()  + distOnBottom * parent.edgeDirBottom();
    }
    inline Amg::Vector2D RadialStripDesign::stripEdges::topMounting() const {
        return parent.cornerTopLeft() + distOnTop * parent.edgeDirTop();
    }
    inline Amg::Vector2D RadialStripDesign::stripEdges::fromBottomToTop() const {
        return topMounting() - bottomMounting();
    }

    inline Amg::Vector2D RadialStripDesign::stripLeftBottom(int stripNumber) const{
        CHECK_STRIPRANGE(stripNumber);
        return m_strips[stripCh].bottomMounting();
    }
    inline Amg::Vector2D RadialStripDesign::stripRightBottom(int stripNumber) const{
        CHECK_STRIPRANGE(stripNumber);
        return m_strips[stripCh+1].bottomMounting();
    }
    inline Amg::Vector2D RadialStripDesign::stripLeftTop(int stripNumber) const{
        CHECK_STRIPRANGE(stripNumber);
        return m_strips[stripCh].topMounting();
    }
    inline Amg::Vector2D RadialStripDesign::stripRightTop(int stripNumber) const{
        CHECK_STRIPRANGE(stripNumber);
        return m_strips[stripCh+1].topMounting();
    }

    inline int RadialStripDesign::stripNumber(const Amg::Vector2D& extPos) const {        
        if (!insideTrapezoid(extPos)) {
            ATH_MSG_VERBOSE("The point "<<Amg::toString(extPos)<<" is outside the active trapezoid area");
            return -1;
        }
        const Eigen::Rotation2D normalRot{(m_reversedStripOrder ? 90 : -90)*Gaudi::Units::deg};
        stripEdgeVecItr itr = std::lower_bound(m_strips.begin(), m_strips.end(), extPos,
                    [& normalRot](const stripEdges& stripPos, const Amg::Vector2D& pos) {
                      const Amg::Vector2D dir = stripPos.fromBottomToTop().unit();
                      const Amg::Vector2D normal = normalRot * dir;
                      std::optional<double> distFromBotEdge = Amg::intersect<2>(stripPos.bottomMounting(), dir, pos, normal);
                      return distFromBotEdge.value_or(1.) < 0;
                    });        
        
        if (itr == m_strips.end() || m_strips.begin() == itr) {
            return -1;
        }
        return std::distance(m_strips.begin(), itr) -1 + firstStripNumber();
    }
} 
#undef CHECK_STRIPRANGE
#endif
