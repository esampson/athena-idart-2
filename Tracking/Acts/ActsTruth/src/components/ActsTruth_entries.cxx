/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../ClusterToTruthAssociation.h"
#include "../TrackToTruthAssociationAlg.h"
#include "../TruthParticleHitCountAlg.h"
#include "../TrackFindingValidationAlg.h"

// Algorithms
DECLARE_COMPONENT( ActsTrk::PixelClusterToTruthAssociationAlg )
DECLARE_COMPONENT( ActsTrk::StripClusterToTruthAssociationAlg )
DECLARE_COMPONENT( ActsTrk::TrackToTruthAssociationAlg )
DECLARE_COMPONENT( ActsTrk::TruthParticleHitCountAlg )
DECLARE_COMPONENT( ActsTrk::TrackFindingValidationAlg )
