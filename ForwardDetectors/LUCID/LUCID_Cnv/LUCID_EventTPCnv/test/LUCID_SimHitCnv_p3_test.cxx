/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
/**
 * @file LUCID_EventTPCnv/test/LUCID_SimHitCnv_p3_test.cxx
 * @date Feb, 2018
 * @brief Tests for LUCID_SimHitCnv_p3.
 */


#undef NDEBUG
#include "LUCID_EventTPCnv/LUCID_SimHitCnv_p3.h"
#include "CxxUtils/checker_macros.h"
#include "TestTools/leakcheck.h"
#include <cassert>
#include <iostream>

#include "TruthUtils/MagicNumbers.h"
#include "GeneratorObjectsTPCnv/initMcEventCollection.h"
#include "AtlasHepMC/GenEvent.h"
#include "AtlasHepMC/GenParticle.h"
#include "AtlasHepMC/Operators.h"


void compare (const HepMcParticleLink& p1,
              const HepMcParticleLink& p2)
{
  assert ( p1.isValid() == p2.isValid() );
  assert ( p1.barcode() == p2.barcode() );
  assert ( p1.id() == p2.id() );
  assert ( p1.eventIndex() == p2.eventIndex() );
  assert ( p1.cptr() == p2.cptr() );
  assert ( p1 == p2 );
}


void compare (const LUCID_SimHit& p1,
              const LUCID_SimHit& p2)
{
  assert (p1.GetTubeID() == p2.GetTubeID());
  assert (p1.GetPdgCode() == p2.GetPdgCode());
  assert (p1.truthBarcode() == p2.truthBarcode());
  assert (p1.truthID() == p2.truthID());
  compare (p1.particleLink(), p2.particleLink());
  assert (p1.particleLink() == p2.particleLink());
  assert (p1.GetGenVolume() == p2.GetGenVolume());
  assert (p1.GetX() == p2.GetX());
  assert (p1.GetY() == p2.GetY());
  assert (p1.GetZ() == p2.GetZ());
  assert (p1.GetEPX() == p2.GetEPX());
  assert (p1.GetEPY() == p2.GetEPY());
  assert (p1.GetEPZ() == p2.GetEPZ());
  assert (p1.GetPreStepTime() == p2.GetPreStepTime());
  assert (p1.GetPostStepTime() == p2.GetPostStepTime());
  assert (p1.GetWavelength() == p2.GetWavelength());
  assert (p1.GetEnergy() == p2.GetEnergy());
}


void testit (const LUCID_SimHit& trans1)
{
  MsgStream log (nullptr, "test");
  LUCID_SimHitCnv_p3 cnv;
  LUCID_SimHit_p3 pers;
  cnv.transToPers (&trans1, &pers, log);
  LUCID_SimHit trans2;
  cnv.persToTrans (&pers, &trans2, log);

  compare (trans1, trans2);
}


void test1 ATLAS_NOT_THREAD_SAFE (std::vector<HepMC::GenParticlePtr>& genPartVector)
{
  std::cout << "test1\n";
  auto particle = genPartVector.at(0);
  // Create HepMcParticleLink outside of leak check.
  HepMcParticleLink dummyHMPL(HepMC::uniqueID(particle),particle->parent_event()->event_number(),
                              HepMcParticleLink::IS_EVENTNUM,HepMcParticleLink::IS_ID);
  assert(dummyHMPL.cptr()==particle);
  Athena_test::Leakcheck check;

  HepMcParticleLink trkLink(HepMC::uniqueID(genPartVector.at(0)),genPartVector.at(0)->parent_event()->event_number(),
                              HepMcParticleLink::IS_EVENTNUM,HepMcParticleLink::IS_ID);
  LUCID_SimHit trans1 (1, genPartVector.at(0)->pdg_id(), trkLink, 4,
                       5.5, 6.5, 7.5,
                       8.5, 9.5, 10.5,
                       11.5, 12.5, 13.5, 14.5);

  testit (trans1);
}


int main ATLAS_NOT_THREAD_SAFE ()
{
  ISvcLocator* pSvcLoc = nullptr;
  std::vector<HepMC::GenParticlePtr> genPartVector;
  if (!Athena_test::initMcEventCollection(pSvcLoc,genPartVector)) {
    std::cerr << "This test can not be run" << std::endl;
    return 0;
  }

  test1(genPartVector);
  return 0;
}
